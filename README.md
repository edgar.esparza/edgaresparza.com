# edgar-esparza

[![Netlify Status](https://api.netlify.com/api/v1/badges/94f39cf2-6829-4977-b1aa-2175743bb785/deploy-status)](https://app.netlify.com/sites/sad-franklin-bd24a5/deploys)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
