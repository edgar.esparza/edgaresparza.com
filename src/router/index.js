import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import ThankYou from "../views/ThankYou.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/thank-you",
    name: "ThankYou",
    component: ThankYou
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
